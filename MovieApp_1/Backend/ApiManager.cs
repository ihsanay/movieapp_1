﻿using System;
using System.Threading.Tasks; //sağ tık, Resolve
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MovieApp_1
{
	public class ApiManager
	{
		/*
		public ApiManager ()
		{

		}*/


		private static Uri sBaseAddress = new Uri("http://api.themoviedb.org/3/"); //dersten kopyaladık

		private static string API_KEY = "7df25bca9d2d6dc283c66ffd5ae86483"; //dersten kopyaladık, tırnakları elle girdik

		public static async Task<List<Movie>> GetPopularMovies() //async: internet yavaş olsa bile senkron bi şekilde..
		{
			//garbage controller temizlesin diye using şeklinde yazdık.
			using (var httpClient = new HttpClient{ BaseAddress = sBaseAddress }) //paketlerden http yazıp ekledik.
			{

				httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

				using(var response = await httpClient.GetAsync("movie/popular?api_key=" + API_KEY)) //siteden alırsak, isteğin sonuna ?... ile keyi belirtmek gerekiyor.
				{
					string responseData = await response.Content.ReadAsStringAsync();

					var result = JsonConvert.DeserializeObject<MovieResponse>(responseData);

					return result.results;
				}
			}
		}
	}
}

