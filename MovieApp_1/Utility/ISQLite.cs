﻿using System;
using SQLite.Net;

namespace MovieApp_1
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

