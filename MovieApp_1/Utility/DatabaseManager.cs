﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;

namespace MovieApp_1
{
	public class DatabaseManager
	{
			private static DatabaseManager instance;
			public static DatabaseManager Instance
			{
				get
				{
					if (instance == null) {
						instance = new DatabaseManager();
					}
					return instance;
				}
			}
		SQLiteConnection database;

		public DatabaseManager ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<Movie> (); //daha önce yaratıldıysa bağlantıyı kuracak
			database.CreateTable<Titles>();
		}


		public Titles GetTitles()
		{
			return database.Table<Titles> ().FirstOrDefault ();
		}

		public int AddTitles(Titles t)
		{
			return t._id == 0 ? database.Insert (t) : database.Update (t);
		}


		public int AddMovie(Movie movie) //void i int yaptık
		{
			return movie._id == 0 ? database.Insert (movie) : database.Update (movie); //yeni birşeyse 0 dan başlatıyoruz, değilse update)
			//database.Insert (movie); //insert te int belirtmesinin sebebi movie nin id sini kullanıyor.
		}

		public Movie GetMovie(int id)
		{
			return database.Get<Movie> (id);
		}

		public Movie[] GetMovies()
		{
			return database.Table<Movie> ().ToArray ();
		}
		
	}
}

