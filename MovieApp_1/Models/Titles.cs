﻿using System;
using SQLite.Net.Attributes;

namespace MovieApp_1
{
	public class Titles
	{
		[PrimaryKey, AutoIncrement]
		public int _id { get; set; }
		public string titleWelcomePage { get; set; }
		public string titleBoxPage { get; set; }
	}
}

