﻿using System;
using System.Collections.Generic;
using SQLite.Net.Attributes;

namespace MovieApp_1
{
	public class Movie
	{
		[PrimaryKey, AutoIncrement]
		public int _id {get;set;}


		public bool adult { get; set; }
		public string backdrop_path { get; set; }
		//public List<int> genre_ids { get; set; } //list e sağ click, resolve dan seç. list ler ve array ler sql eklenmez
		public int id { get; set; }
		public string original_language { get; set; }
		public string original_title { get; set; }
		public string overview { get; set; }
		public string release_date { get; set; }
		public string poster_path { get; set; }
		public double popularity { get; set; }
		public string title { get; set; }
		public bool video { get; set; }
		public double vote_average { get; set; }
		public int vote_count { get; set; }
	}

	public class MovieResponse// RootObject'i MovieResponse olarak değştirdik
	{
		public int page { get; set; }
		public List<Movie> results { get; set; } //result u Movie yaptık
		public int total_pages { get; set; }
		public int total_results { get; set; }
	}
}

