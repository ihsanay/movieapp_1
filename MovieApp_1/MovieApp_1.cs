﻿using System;

using Xamarin.Forms;
using System.Reflection;
using Newtonsoft.Json;
using System.IO;

namespace MovieApp_1
{
    public class App : Application
    {
        public App()
        {

			Util.tTitles = DatabaseManager.Instance.GetTitles();
			if (Util.tTitles == null)
			{
				Assembly assembly = typeof(App).GetTypeInfo().Assembly;
				Stream stream = assembly.GetManifestResourceStream("MovieApp_1.Assets.strings.json");
				string text = "";
				using (var reader = new System.IO.StreamReader(stream))
				{
					text = reader.ReadToEnd();
				}
				var result = JsonConvert.DeserializeObject<Titles>(text);
				DatabaseManager.Instance.AddTitles(result);
				Util.tTitles = DatabaseManager.Instance.GetTitles();
			}

            // The root page of your application
            MainPage = new MainPage();


        }

        protected override void OnStart()
        {
            // Handle when your app starts

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

