﻿using System;

using Xamarin.Forms;

namespace MovieApp_1
{
	public class MovieListCell : ViewCell
	{
		public MovieListCell ()
		{
			Image imgPoster = new Image {
				WidthRequest = 100,
				HeightRequest = 125,
				Aspect = Aspect.AspectFit
			};

			imgPoster.SetBinding (Image.SourceProperty, "ImageSource");
			Label lblTitle = new Label {
				TextColor = Color.Black,
				FontSize = 18,
				XAlign = TextAlignment.Start
				//böyle text = ... yazmak yerine dışarıdan gelmesi için aşağıdaki kodu kullandık.
			};

			lblTitle.SetBinding (Label.TextProperty, "Title");

			var lblReleaseDateTitle = new Label {
				TextColor = Color.Black,
				FontSize = 14,
				XAlign = TextAlignment.Start,
				//Text değerini kendimiz burada gireceğimiz, dışardan almayacağımız için aşağıda belirtiyoruz.
				Text = "Release Date: "	
			};

			var lblReleaseDate = new Label
			{
				TextColor = Color.Black,
				FontSize = 14,
				XAlign = TextAlignment.Start
			};

			lblReleaseDate.SetBinding (Label.TextProperty, "ReleaseDate");

			StackLayout stckReleaseDate = new StackLayout 
			{
				Orientation = StackOrientation.Horizontal,
				Spacing = 2,
				Children = {
					lblReleaseDateTitle,
					lblReleaseDate
				}
			};

			var stckInfo = new StackLayout {
				Spacing = 10,
				Padding = 6,
				Children = {
					lblTitle,
					stckReleaseDate
				}
			};

			var stckRoot = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 7,
				Padding = 7,
				Children = {
					imgPoster,
					stckInfo
				}
			};

			this.View = stckRoot;
		}
	}
}


