﻿using System;
using Xamarin.Forms;

namespace MovieApp_1
{
	public class MovieListCellHolder
	{
		//prop yazıp tab tab yaptık kendi geldi.
		public string Title {get;set;		}
		public string ReleaseDate {	get;set;		}
		public UriImageSource ImageSource{ get; set; } //UriImageSource sağ tıklayıp resolve dan seçtik
	}
}

