﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MovieApp_1
{
    public class BoxPage : ContentPage
    {
		List<Movie> lstMovies = new List<Movie>();
		ListView lvMovie;

        public BoxPage()
        {
			Title = "Favorites";
				lvMovie = new ListView
			{
				VerticalOptions = LayoutOptions.FillAndExpand
			};
				lvMovie.ItemTemplate = new DataTemplate(typeof(MovieListCell));
				lvMovie.RowHeight = 130;
				Device.BeginInvokeOnMainThread(() =>
					{
						lstMovies = DatabaseManager.Instance.GetMovies().ToList();
							lvMovie.ItemsSource = null;
							lvMovie.ItemsSource = lstMovies.Select(x =>
								{
									return new MovieListCellHolder{
										ImageSource = new UriImageSource { Uri = Util.GetImageUri(x.poster_path)},
										Title = x.title,
										ReleaseDate = x.release_date
									};
								}).ToArray();
					});
				lvMovie.ItemTapped += (sender, e) =>
			{
				var item = e.Item as MovieListCellHolder;
					var movieCurr = lstMovies.FirstOrDefault(x => x.title == item.Title);
					Navigation.PushModalAsync(new NavigationPage(new MovieDetailPage(movieCurr)));
			};
				Content = new StackLayout
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.Center,
				Children =
				{
					lvMovie
				}
			};
		
        }

		protected override void OnAppearing ()
		{
			Device.BeginInvokeOnMainThread(() =>
				{
					lstMovies = DatabaseManager.Instance.GetMovies().ToList();
					lvMovie.ItemsSource = null;
					lvMovie.ItemsSource = lstMovies.Select(x =>
						{
							return new MovieListCellHolder{
								ImageSource = new UriImageSource { Uri = Util.GetImageUri(x.poster_path)},
								Title = x.title,
								ReleaseDate = x.release_date
							};
						}).ToArray();
				});
					
			base.OnAppearing ();
		}


    }
}


