﻿using System;

using Xamarin.Forms;

namespace MovieApp_1
{
    public class MainPage : ContentPage
    {
        public MainPage()
        {
            Label lblWelcome = new Label
            {
                Text = "Welcome my friend!",
                TextColor = Color.Red,
                XAlign = TextAlignment.Center
            };

            Entry eUserName = new Entry
            {
                WidthRequest = 150,
                HeightRequest = 50,
                Placeholder = "Please enter your name..",
            };

            Button btnInit = new Button
            {
                Text = "Login",
                WidthRequest = 150,
                HeightRequest = 50,
                BackgroundColor = Color.Aqua,
                TextColor = Color.Black
            };

            btnInit.Clicked += (sender, e) => 
            {
                Navigation.PushModalAsync(new TabPage(eUserName.Text));

//                if(!string.IsNullOrEmpty(eUserName.Text))
//                    lblWelcome.Text = eUserName.Text;
//                else
//                    lblWelcome.Text  = "Please enter a text!!";
            };     


            Content = new StackLayout
            { 
                Children =
                {
                    lblWelcome,
                    eUserName,
                    btnInit
                },
                    Padding = 20,
                    Spacing = 10
            };
        }

    }
}


