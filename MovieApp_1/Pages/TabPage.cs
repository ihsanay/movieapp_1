﻿using System;

using Xamarin.Forms;

namespace MovieApp_1
{
    public class TabPage : TabbedPage
    {
        public TabPage(string Text)
        {
            this.Children.Add(new WelcomePage(Text));
            this.Children.Add(new BoxPage());
        }
    }
}


