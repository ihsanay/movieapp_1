﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MovieApp_1
{
    public class WelcomePage : ContentPage
    {
		List<Movie> lstMovies = new List<Movie>();
		ListView lvMovie;

        public WelcomePage(string sText)
        {
            this.Title = "Welcome";

			lvMovie = new ListView {
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			lvMovie.ItemTemplate = new DataTemplate (typeof(MovieListCell));
			lvMovie.RowHeight = 130;

			Device.BeginInvokeOnMainThread (async() => {
				lstMovies = await ApiManager.GetPopularMovies(); //async olduğu için await yapmak lazım ama constructer da await yapamayız. 

				lvMovie.ItemsSource = null; //bazen sıkıntı çıkarabildiği için bunu ekliyoruz.

				lvMovie.ItemsSource  = lstMovies.Select(x =>
					{
						return new MovieListCellHolder //her bir film için holder yarat
						{
							Title = x.title,
							ReleaseDate = x.release_date,
							ImageSource = new UriImageSource{Uri = Util.GetImageUri(x.poster_path)} //{Uri = new Uri("https://image.tmdb.org/t/p/w185" + x.poster_path)}
						};
					}).ToArray();
				
				// daha sonra değiştirecez
				lvMovie.ItemTapped += (sender, e) =>
				{
					var item = e.Item as MovieListCellHolder;
					var movieCurr = lstMovies.FirstOrDefault(x => x.title == item.Title);
					Navigation.PushModalAsync(new NavigationPage(new MovieDetailPage(movieCurr)));
				};

//					Firstly with “var item = e.Item”, we get the clicked item’s information. We used 
//					MovieListCellHolder as our ItemSource, so when we click a listview item, the return type of e.Item 
//					will be the ItemSource’s Type. So e.Item’s type is MovieListCellHolder now. 
//					Then I want to get the movie data of the selected item. So we use a linq query to get the movie 
//					data out of the movie list. It means “Get me the first item that has the same title as this item’s title”.
//					Then we create our DetailPage and pass in the information. We must however “encapsulate” this 
//					page in a new NavigationPage, because we wanted a back arrow. So we set our MovieDetailPage 
//					as it’s parameter.
//					So in one line we are creating a new NavigationPage, that has a new MovieDetailPage, that has 
//					our information.
//					It’s done. Let’s not forget git. Then you can press the play button and see the detail page!






				//lvMovie.ItemsSource = lstMovies.Select(x => x.title).ToArray(); // listenin her elemanın title'ını select yap.
			});


			//derste geçici tıklayınca kayıt için yapmıştık
//			lvMovie.ItemTapped += (sender, e) => {
//				var movie = e.Item as MovieListCellHolder; //item in bilgileri aldık
//				var item = lstMovies.FirstOrDefault(x=>x.title == movie.Title); 
//
//				//DatabaseManager.Instance.AddMovie (item); //dıştaydı içe aldım.
//			};



            Content = new StackLayout
            { 
				VerticalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                   //new Label { Text = string.IsNullOrEmpty(sText)? "Hello!" : sText  } // bunu siliyouz
					lvMovie
                }
            };
        }
    }
}


