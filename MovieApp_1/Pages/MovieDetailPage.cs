﻿using System;

using Xamarin.Forms;

namespace MovieApp_1
{
	public class MovieDetailPage : ContentPage
	{
		public MovieDetailPage (Movie movie)
		{
			this.Title = movie.title;


			//We need an Image to show the poster.
			Image imgPoster = new Image
			{
				WidthRequest = 200,
				HeightRequest = 320,
				Source = new UriImageSource{Uri = Util.GetImageUri(movie.poster_path)},
				HorizontalOptions = LayoutOptions.Center
			};
					

			//Then we want to show the overview info of our movie. First just add “OVERVIEW” as the title.
			Label lblOverviewTitle = new Label
			{
				Text = "OVERVIEW",
				FontSize = 18,
				TextColor = Color.White
			};

			//Next we should show the overview, of course :)
			Label lblOverview = new Label
			{
				Text = movie.overview,
				FontSize = 14,
				TextColor = Color.White
			};

			//And let’s create a StackLayout for the overview. Because we want them one under 
			//the other, and as an easily manageable block so we can edit the style.
			StackLayout stckOverview = new StackLayout
			{
				Spacing = 4,
				Padding = 6,
				Children = {
					lblOverviewTitle,
					lblOverview
				},
				BackgroundColor = Color.FromRgb(0,183,211),
				VerticalOptions = LayoutOptions.StartAndExpand
			};


			//Now lastly I want to display the vote average of the movie.
			Label lblVote = new Label
			{
				Text = movie.vote_average.ToString() + " / 10", //6. derse /10 kısmını ekledik
				FontSize = 18,
				TextColor = Color.Black,
				XAlign = TextAlignment.Center
			};



			//17.11.2015
			Button btnFavorite = new Button
			{
				BorderColor = Color.FromRgb(0,183,211),
				BorderRadius = 2,
				BorderWidth = 2,
				Text = "Favorite",
				BackgroundColor = Color.White,
				TextColor = Color.FromRgb(0,183,211)
			};

			StackLayout stckFavGroup = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Padding = 2,
				Children = {
					lblVote,
					btnFavorite
				},
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};



			btnFavorite.Clicked += (sender, e) => {
				DatabaseManager.Instance.AddMovie (movie);
			};







			//And, as before, we need a Stacklayout as our root layout.
			var root = new StackLayout
			{
				Spacing = 4,
				Children =
				{
					imgPoster,
					lblVote,
					stckOverview,
					stckFavGroup
				}
			};



			//And lastly we should assign our root layout as the Content value of this ContentPage.
			Content = new ScrollView
			{
				Content = root
			};

			//I put the root layout in a scroll view. Because we don’t know the
			//length of the overview. It can be 6 lines or 50 lines. So our page should scroll
			//vertically to show all possible content.





			//eski default ürettiği
//			Content = new StackLayout { 
//				Children = {
//					new Label { Text = "Hello ContentPage" }
//			
//				}
//			};
		}
	}
}


