﻿using System;
using Xamarin.Forms;
using MovieApp_1.Droid;
using SQLite.Net;
using System.IO;

[assembly: Dependency(typeof(SQLite_Android))]
namespace MovieApp_1.Droid
{
	public class SQLite_Android : ISQLite
	{
		public SQLite_Android ()
		{

		}

		public SQLiteConnection GetConnection ()
		{
			string databaseFileName = "MoviesSqLite.db3";

			string documentsFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);

			var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();

			var path = Path.Combine (documentsFolder, databaseFileName);

			var connection = new SQLiteConnection (platform, path);

			return connection;
		}
	}
}

